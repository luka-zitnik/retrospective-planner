function setUpClockEvents(clock, degreesPerSecond) {

  var mousedown = false,
          intervalID;

  // In terms of Retrospective Planner, if the test passes, there are no planned activities
  if (degreesPerSecond === Infinity) {
    return;
  }

  document.getElementById("play-btn").onclick = function () {

    window.clearInterval(intervalID);

    intervalID = window.setInterval(function () {

      var newAngle;

      if (clock.endAngle === 360) {
        window.clearInterval(intervalID);
        return;
      }

      newAngle = clock.endAngle + degreesPerSecond;

      if (newAngle > 360) {
        clock.updateEndAngle(360);
      }
      else {
        clock.updateEndAngle(newAngle);
      }
    }, 1000);

    clock.onupdate(); // Trigger current class assignment

  };

  document.getElementById("pause-btn").onclick = function () {
    window.clearInterval(intervalID);
  };

  document.getElementById("reset-btn").onclick = function () {
    window.clearInterval(intervalID);
    clock.updateEndAngle(0);
  };

  clock.path.ownerSVGElement.addEventListener("mousedown", function (event) {
    event.preventDefault(); // Prevents the dragstart event
    mousedown = true;
  });

  clock.path.ownerSVGElement.addEventListener("mousemove", function (event) {
    var ctm,
            screenPoint,
            svgPoint;

    if (mousedown) {

      screenPoint = event.currentTarget.createSVGPoint();
      screenPoint.x = event.clientX;
      screenPoint.y = event.clientY;

      ctm = event.currentTarget.getScreenCTM();

      svgPoint = screenPoint.matrixTransform(ctm.inverse());

      clock.updateEndAngle(parseInt(clock.pointToAngle(svgPoint)));
    }
  });

  clock.path.ownerSVGElement.addEventListener("mouseup", function (event) {
    mousedown = false;
  });

  clock.path.ownerSVGElement.addEventListener("touchmove", function (event) {
    var ctm,
            screenPoint,
            svgPoint;

    screenPoint = event.currentTarget.createSVGPoint();
    screenPoint.x = event.touches[0].clientX;
    screenPoint.y = event.touches[0].clientY;

    ctm = event.currentTarget.getScreenCTM();

    svgPoint = screenPoint.matrixTransform(ctm.inverse());

    clock.updateEndAngle(parseInt(clock.pointToAngle(svgPoint)));
  });
}

function drawClock(angles, meetingDuration) {

  var MASK_RADIUS = 13,
          PHASES_RADIUS = 20,
          CLOCK_RADIUS = 50,
          CENTER = {x: 50, y: 50};

  var fragment = document.createDocumentFragment(),
          svg = document.getElementById("clock"),
          clock = new SVGCircleSector(CENTER, CLOCK_RADIUS, 0, 0),
          text = SVGElementsGenerator.createSVGText(CENTER.x, CENTER.y),
          meetingInSeconds = meetingDuration * 60,
          i = 0,
          phases = [];

  fragment.appendChild(clock.path);

  for (; i < angles.length; ++i) {
    fragment.appendChild((phases[i] = new SVGCircleSector(
            CENTER, PHASES_RADIUS, angles[i - 1] || 0, angles[i]
            ).path));
  }

  // This should be a mask instead
  fragment.appendChild(SVGElementsGenerator.createSVGWhiteCircle(CENTER, MASK_RADIUS));

  fragment.appendChild(text);

  while(svg.lastChild) {
    svg.removeChild(svg.lastChild);
  }

  svg.appendChild(fragment);

  setUpClockEvents(
          clock,
          360 / meetingInSeconds // Degrees per second
          );

  text.textContent = secondsToTimeString(meetingInSeconds);

  clock.onupdate = function () {

    var i;

    // Display time remaining
    text.textContent = secondsToTimeString(Math.ceil(meetingInSeconds / 360 * (360 - this.endAngle)));

    for (i = 0; i < angles.length; ++i) {
      phases[i].classList.remove("current");
    }

    for (i = 0; i < angles.length; ++i) {
      if (this.endAngle < angles[i]) {
        phases[i].classList.add("current");
        break;
      }
    }
  };
}

function secondsToTimeString(seconds) {
  var hours,
          minutes;

  hours = parseInt(seconds / 3600);
  minutes = parseInt(seconds % 3600 / 60);
  seconds = parseInt(seconds % 60);

  return pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);
}

function pad(number) {
  return ("0" + number).slice(-2);
}

ko.computed(function () {
  var plannedActivities,
          activityDurations,
          meetingDuration,
          anglesPerMinute,
          sectorAngles;

  plannedActivities = viewModel.phases().reduce(function (memo, phase) {
    return memo.concat(phase.plannedActivities());
  }, []);

  activityDurations = _.pluck(plannedActivities, "duration");

  meetingDuration = activityDurations.reduce(function (memo, duration) {
    return memo + duration;
  }, 0);

  anglesPerMinute = 360 / meetingDuration;

  sectorAngles = activityDurations.map(function (duration) {
    return anglesPerMinute * duration;
  });

  // Modify angles to their absolute value
  for (var i = 1; i < sectorAngles.length; ++i) {
    sectorAngles[i] += sectorAngles[i - 1];
  }

  drawClock(sectorAngles, meetingDuration);
});