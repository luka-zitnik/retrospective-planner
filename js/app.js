var phases = [
  {
    name: "Set the Stage",
    activities: [
      {
        name: "Check-In",
        purpose: "Help people put aside other concerns and focus on the retrospective. Help people articulate what they want from the retrospective.",
        minDuration: 5,
        maxDuration: 10,
        description: "After welcoming the participants and reviewing the goal and agenda, the retrospective leader asks one brief question. Each person answers in round-robin fashion.",
        reminders: [
          "Ask one brief question",
          "Let people choose to pass the question",
          "Refrain from evaluative comments"
        ]
      },
      {
        name: "Focus On/Focus Off",
        purpose: "Help establish a mind-set for productive communication. Help participants set aside blaming and judgement - and fear of blaming and judgement.",
        description: "After welcoming the participants and reviewing the goal and agenda, the retrospective leader describes productive and unproductive communication patterns. After describing those patterns, the participants discuss what they mean for the retrospective.",
        minDuration: 8,
        maxDuration: 12,
        reminders: [
          "Present the poster",
          "Form small discussion groups",
          "Each group picks a pair of words to discuss",
          "They report to the whole team",
          "Ask for willingness to stay in the left column"
        ]
      },
      {
        name: "ESVP",
        purpose: "Focus people on the work of the retrospective. Understand people's attitudes to the retrospective.",
        description: "Each person reports (anonymously) his or her attitude toward the retrospective as an Explorer, Shopper, Vacationer, or Prisoner (ESVP). The retrospective leader collects the results and creates a histogram to show the data and then guides a discussion about what the results mean for the group.",
        minDuration: 10,
        maxDuration: 15,
        reminders: [
          "Present the poll",
          "Show the flip chart and explain the terms",
          "Distribute slips of paper or small index cards",
          "Collect and shuffle",
          "Read while another writes tick marks",
          "Tear up the slips and throw them away",
          "Initiate commenting on the data",
          "Choose where to go next"
        ]
      },
      {
        name: "Working Agreements",
        purpose: "Establish a set of behaviors that will support the team in having productive discussions. Establish that team members are responsible for monitoring their interactions. Provide candidates for day-to-day working agreements if the team doesn't already have them.",
        description: "Team members work together to generate ideas for effective behaviors at work then choose five to seven agreements to guide team interactions or processes.",
        minDuration: 10,
        maxDuration: 30,
        reminders: [
          "Explain the activity",
          "Form small groups",
          "Ask each group to develop three to five working agreements",
          "Ask each group to report",
          "Write them down on flip chart",
          "Thumb/dot voting for three to sevent agreements"
        ]
      }
    ]
  },
  {
    name: "Gather the Data",
    activities: [
      {
        name: "Timeline",
        purpose: "Simulate memories of what happened during the increment of work. Create a picture of work from many perspectives. Examine assumptions about who did what when. See patterns or when energy levels changed. Use this for \"just the facts\" or facts and feelings.",
        description: "Group members write cards to represent memorable, personally meningful, or otherwise significant events during the iteration, release, or project and then post them in (roughly) chronological order. The retrospective leader supports the team to discuss the events to understand facts and feelings during the iteration, release, or project.",
        minDuration: 30,
        maxDuration: 90,
        reminders: [
          "Stress the many perspectives",
          "Form affinity groups",
          "Monitor the level of activity",
          "Invite the team to see what others have posted",
          "Call for a break before the next phase"
        ]
      },
      {
        name: "Triple Neckles",
        purpose: "Generate ideas for actions or recommendations. Unceover important topics about the project history.",
        description: "Form small groups. In the groups, each person has five minutes to brain-storm and write down ideas individually. At the end of the five minutes, each person passes the paper to the person on his or her right. That person has five minutes to write down ideas that build on the ideas already written on the paper. Repeat until the paper returns to the original writer.",
        minDuration: 30,
        maxDuration: 60,
        reminders: [
          "Ask them to generate as many ideas as possible",
          "Form small groups",
          "Papers shift after five minute intervals",
          "Each person reads at the end",
          "Debrief with questions",
          "Invite people to post the papers"
        ]
      },
      {
        name: "Color Code Dots",
        purpose: "Show how people experienced events on the timeline.",
        description: "Team members use sticky dots to show events on the timeline where emotions ran high or low.",
        minDuration: 5,
        maxDuration: 20,
        reminders: [
          "Introduce the activity",
          "Provide each with dots in two colors",
          "Each person marks events from the timeline",
          "Investigate events by the dots"
        ]
      },
      {
        name: "Mad Sad Glad",
        purpose: "Get the feeling facts out on the table",
        description: "Individuals use color cards or sticky notes to describe times during the project when they were mad, sad, or glad.",
        minDuration: 20,
        maxDuration: 30,
        reminders: [
          "Introduce the activity",
          "They write down events on sticky notes",
          "Then they post them on the three posters",
          "Cluster by events with their help",
          "Ask them to name each cluster",
          "Debrief with questions"
        ]
      },
      {
        name: "Locate Strengths",
        purpose: "Indentify strengths so the team can build on them in the next iteration. Proivde balance when an iteration, release, or project hasn't gone well.",
        description: "Team members interview each other about high points on the project. The goal is to understand the sources and circumstances that created those high points.",
        minDuration: 15,
        maxDuration: 40,
        reminders: [
          "Introduce the activity",
          "Form pairs",
          "Hand out the interview questions",
          "Explain the interview process",
          "Start another interview with roles switched",
          "Continue with Indentify Themes activity"
        ]
      },
      {
        name: "Satisfaction Histogram",
        purpose: "Highlight how satisfied team members are with a focus area. Provide a visual picture of current status in a particular area to help the team have deeper discussions and analysis. Acknowledge differences in perspective among team members.",
        description: "Team members use a histogram to gauge individual and group satisfaction with practices and process.",
        minDuration: 5,
        maxDuration: 10,
        reminders: [
          "Note histogram's reusability",
          "Read definitions",
          "People vote anonymously",
          "A volounteer colors the graph",
          "Read the results",
          "Ask for comments"
        ]
      },
      {
        name: "Team Radar",
        purpose: "Help the team gauge how well they are doing on a variety of measures, such as, engineering practices, team values, or other processes.",
        description: "Team members track individual and group ratings for specific factors about process or development practices they want to examine.",
        minDuration: 15,
        maxDuration: 20,
        reminders: [
          "Start off with blank radar graph",
          "Ask each person to place a dot mark for each factor",
          "Lead a short discussion",
          "Save completed graph for later comparison"
        ]
      }
    ]
  },
  {
    name: "Generate Insights",
    activities: [
      {
        name: "Brainstorming/Filtering",
        purpose: "Generate a large number of ideas and filter them against a defined set of criteria.",
        description: "Team members generate ideas using traditional brainstorming, then test whether each idea is applicable to the current situation.",
        minDuration: 40,
        maxDuration: 60,
        reminders: [
          "Ask them to go beyond usual thinking",
          "Describe brainstorming guidelines",
          "Three methods",
          "Ask what filters",
          "Show of hands for the top four filters",
          "Ask group which ideas to carry forward"
        ]
      },
      {
        name: "Force Field Analysis",
        purpose: "To examine what factors in the organization will support a proposed change and which will inibit the change.",
        description: "The team defines a desired state they want to achive. Small groups work to identify the factors that could either restrain or drive the change they want. The factors are listed on a poster; then the group assesses the strength of each supporting factor relative to the other supporting factors and repeats the process for inhibiting factors. The team discusses which factors they can influence - either by increasing the strength of a supporing factor or by reducing the strength of an inhibition factor.",
        minDuration: 45,
        maxDuration: 60,
        reminders: [
          "Introduce activity",
          "Break into small groups",
          "Groups indentify supporting factors",
          "Groups report and post results in round-robin fashion",
          "Repeat for restraining or inhibiting factors",
          "Examine the factors for most effective actions"
        ]
      }
    ]
  },
  {
    name: "Decide What to Do",
    activities: [
      {
        name: "Retrospective Planning Game",
        purpose: "Develop detailed plans for experiments or proposals",
        description: "Team members work individually or in pairs to brainstorm all the tasks necessary to complete an experiment, improvement, or recommendation. After brainstorming, team members eliminate redundant tasks and fill in gaps. The tasks are arranged in order, and team members sign up for tasks they will complete.",
        minDuration: 40,
        maxDuration: 75,
        reminders: [
          "Stress tasks are the autput",
          "Pairs to generate tasks",
          "Pairs of pairs to compare, remove duplicates and fill gaps",
          "Which task must be done next?",
          "Which can be done simultaneously with this task?",
          "Invite team members to sign up for tasks"
        ]
      }
    ]
  },
  {
    name: "Close Retrospective",
    activities: [
      {
        name: "+/Delta",
        purpose: "To retrospect on the retrospective and identify strengths and improvements.",
        description: "The team indentifies the strengths (do more of) and changes for the next retrospective.",
        minDuration: 10,
        maxDuration: 20,
        reminders: [
          "Draw a \"T\" on a flip chart",
          "Ask the group to shout out strengths and changes",
          "Thank the group",
          "Discover patterns"
        ]
      }
    ]
  }
];

var viewModel = {
  phases: ko.observableArray(phases)
};

(function () {
  for (var phase in viewModel.phases()) {
    viewModel.phases()[phase].plannedActivities = ko.observableArray([]);
  }
})();

var db = new Dexie("retrospectivePlanner");

db.version(1).stores({
  plannedActivities: "++id,phase,activity"
});

db.on("error", function (error) {
  alert("Ooops: " + error);
});

db.open();

db.plannedActivities.toArray(function (dbData) {
  for (var i = 0; i < dbData.length; ++i) {
    var phase = _.findWhere(viewModel.phases(), {name: dbData[i].phase});

    phase.plannedActivities.push({
      name: dbData[i].activity,
      duration: dbData[i].duration,
      reminders: _.findWhere(phase.activities, {name: dbData[i].activity}).reminders,
      id: dbData[i].id
    });
  }
});

viewModel.newActivityViewShouldBeVisible = ko.observable(false);
viewModel.editActivityViewShouldBeVisible = ko.observable(false);

viewModel.availableActivities = ko.observableArray();

viewModel.selectedPhase = ko.observable();
viewModel.selectedActivity = ko.observable();
viewModel.selectedDuration = ko.observable();

viewModel.editingActivity = null;

viewModel.possibleDurations = ko.computed(function () {
  if (viewModel.selectedActivity() === undefined) {
    return;
  }

  return _.range(
          viewModel.selectedActivity().minDuration,
          viewModel.selectedActivity().maxDuration + 1
          );
});

viewModel.openNewActivityView = function () {
  viewModel.newActivityViewShouldBeVisible(true);
  viewModel.availableActivities(_.findWhere(viewModel.phases(), {name: this.name}).activities);
  viewModel.selectedPhase(this);
};

viewModel.openEditActivityView = function (activity) {
  viewModel.editActivityViewShouldBeVisible(true);
  viewModel.availableActivities(_.findWhere(viewModel.phases(), {name: this.name}).activities);
  viewModel.selectedPhase(this);
  viewModel.selectedActivity(_.findWhere(this.activities, {name: activity.name}));
  viewModel.selectedDuration(activity.duration);
  viewModel.editingActivity = activity;
};

viewModel.closeNewActivityView = function () {
  this.newActivityViewShouldBeVisible(false);
};

viewModel.closeEditActivityView = function () {
  this.editActivityViewShouldBeVisible(false);
};

viewModel.addNewActivity = function () {
  var phase = this.selectedPhase(),
          activity = this.selectedActivity(),
          duration = this.selectedDuration();

  db.plannedActivities.add({
    phase: phase.name,
    activity: activity.name,
    duration: duration
  }).then(function (id) {
    phase.plannedActivities.push({
      name: activity.name,
      duration: duration,
      reminders: activity.reminders,
      id: id
    });
  });

  this.closeNewActivityView();
};

viewModel.finishEditingActivity = function () {
  var phase = this.selectedPhase(),
          activity = this.selectedActivity(),
          duration = this.selectedDuration(),
          editingActivity = this.editingActivity;

  db.plannedActivities.update(editingActivity.id, {
    activity: activity.name,
    duration: duration
  }).then(function (result) {
    if (result === 1) {

      // Splice, instead of property update, so KO could update the DOM

      phase.plannedActivities.splice(phase.plannedActivities.indexOf(editingActivity), 1, {
        name: activity.name,
        duration: duration,
        reminders: activity.reminders,
        id: editingActivity.id
      });
    }
  });

  this.closeEditActivityView();
};

viewModel.deleteActivity = function (activity) {
  var answer = window.confirm("Are you sure you want to delete " + activity.name + "?"),
          plannedActivities = this.plannedActivities;

  if (answer === true) {
    db.plannedActivities.delete(activity.id).then(function () {
      plannedActivities.splice(plannedActivities.indexOf(activity), 1);
    });
  }
};

viewModel.formatMinutes = function (n) {
  return n + " minutes";
};

viewModel.clockViewShouldBeVisible = ko.observable(false);

viewModel.openClockView = function () {
  this.clockViewShouldBeVisible(true);
};

viewModel.closeClockView = function () {
  this.clockViewShouldBeVisible(false);
};

ko.applyBindings(viewModel);
