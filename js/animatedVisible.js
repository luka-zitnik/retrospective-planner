ko.bindingHandlers.animatedVisible = {
  bound: [],
  update: function (element, valueAccessor, allBindings) {
    var self = ko.bindingHandlers.animatedVisible,
            animations;

    if (self.bound.indexOf(element) === -1) {
      self.bound.push(element);
      ko.unwrap(valueAccessor());
      return;
    }

    animations = allBindings.get("animations");

    if (ko.unwrap(valueAccessor())) {
      element.classList.remove(animations.hide);
      element.classList.add(animations.show);
    }
    else {
      element.classList.remove(animations.show);
      element.classList.add(animations.hide);
    }
  }
};