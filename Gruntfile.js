module.exports = function (grunt) {

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-copy-includes");
  grunt.loadNpmTasks("grunt-vector2raster");
  grunt.loadNpmTasks("grunt-contrib-compress");

  grunt.initConfig({

    clean: {
      main: ["build", "dist"]
    },

    copy: {
      main: {
        expand: true,
        src: "manifest.webapp",
        dest: "build"
      }
    },

    copy_includes: {
      main: {
        src: "index.html",
        dest: "build"
      }
    },

    vector2raster: {
      main: {
        options: {
          dest: [
            {name: "icon-512.png", size: 512},
            {name: "icon-128.png", size: 128},
            {name: "icon-60.png", size: 60}
          ]
        },
        files: [
          {src: "icon.svg", dest: "build/img/icons/"}
        ]
      }
    },

    compress: {
      main: {
        options: {
          archive: "dist/retrospective-planner.zip"
        },
        expand: true,
        cwd: "build",
        src: "**"
      }
    }
  });

  grunt.registerTask("default", ["clean", "copy", "copy_includes", "vector2raster", "compress"]);
};